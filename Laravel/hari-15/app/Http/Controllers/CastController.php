<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cast;

class CastController extends Controller
{
    public function cast(){
        // $casts = DB::table('cast')->get();
        $casts = Cast::all();
        return view('cast', compact('casts'));
    }

    public function create(){
        return view('create');
    }

    public function show($id){
        // $cast = DB::table('cast')->where('id', $id)->first();
        $cast = Cast::find($id);
        return view('show', compact('cast'));
    }
    
    public function edit($id){
        // $cast = DB::table('cast')->where('id', $id)->first();
        $cast = Cast::find($id);
        return view('edit', compact('cast'));
    }

    public function destroy($id){
        // $cast = DB::table('cast')->where('id', $id)->delete();
        Cast::destroy($id);
        return redirect('/cast')->with('success', "Berhasil Delete Cast!");
    }

    public function update($id, Request $request){

        $request->validate([
            "nama" => 'required|unique:cast',
            "umur" => 'required',
            "bio" => 'required'
        ]);

        // $cast = DB::table('cast')
        // ->where('id', $id)
        // ->update([
        //     'nama'=> $request['nama'],
        //     'umur'=> $request['umur'],
        //     'bio'=> $request['bio']
        // ]);
        $cast = Cast::where('id', $id)->update([
            'nama'=> $request['nama'],
            'umur'=> $request['umur'],
            'bio'=> $request['bio']
        ]);

        return redirect('/cast')->with('success', 'Berhasil Update Post!');
    }

    public function store(Request $request){

        $request->validate([
            "nama" => 'required|unique:cast',
            "umur" => 'required',
            "bio" => 'required'
        ]);

        //without eloquent
        /*$query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);*/

        //eloquent first method
        /*$cast = new Cast;
        $cast->nama = $request["nama"];
        $cast->umur = $request["umur"];
        $cast->bio = $request["bio"];
        $cast->save();*/

        //eloquent second method
        $cast = Cast::create([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);

        return redirect('/cast')->with('success', 'Cast Berhasil disimpan!');

    }
}
