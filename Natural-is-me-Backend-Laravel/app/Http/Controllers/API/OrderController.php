<?php

namespace App\Http\Controllers\API;

use App\Events\OrderStored;
use App\Models\Cart;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Mail\AdminProduct;
use App\Mail\ProductBuyer;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    public function index(){
        $order = Order::where('user_id', Auth::user()->id)->with('products')->get();

        return response()->json([
            'error' => false,
            'message' => "Sukses",
            'data' => $order
        ]);
    }


    public function store(Request $request)
    {
        $carts = Cart::where('user_id', Auth::user()->id)->with('product', 'package')->get();

        $validator = Validator::make($request->all(), [
            'nama_penerima' => 'required|max:255|string',
            'handphone' => 'required|numeric',
            'provinsi' => 'required|max:255|string',
            'kota' => 'required|max:255|string',
            'kecamatan' => 'required|max:255|string',
            'kodepos' => 'required|max:255|string',
            'alamat' => 'required|max:255|string'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => 'The given data was invalid',
                'data'=> $this->transform($validator)
            ], 422);
        }

        if (!Cart::where('user_id', Auth::user()->id)->with('product')->exists()) {
            return response()->json([
                'error' => true,
                'message' => 'Cart Kosong'
            ], 422);
        }

        $products = [];
        $packages = [];
        $total = 0;

        foreach($carts as $cart){
            if($cart->product == null){
                $packages[$cart->package_id]['qty'] = $cart->qty;
                $total += $cart->package->harga * $cart->qty;
            }else{
                $products[$cart->product_id]['qty'] = $cart->qty;
                $total += $cart->product->harga * $cart->qty;
            }
        }

        $order = new Order();
        $order->nama_penerima = $request->nama_penerima;
        $order->handphone = $request->handphone;
        $order->provinsi = $request->provinsi;
        $order->kota = $request->kota;
        $order->kecamatan = $request->kecamatan;
        $order->kodepos = $request->kodepos;
        $order->alamat = $request->alamat;
        $order->harga_total = $total;
        $order->ongkir = "10000";
        $order->courier_id = 1;
        $order->user_id = Auth::user()->id;
        $order->save();

        event(new OrderStored($order));
        
        $order->products()->attach($products);
        $order->packages()->attach($packages);
        
        $deleteCart = Cart::where('user_id', Auth::user()->id)->delete();

        return response()->json([
            'error' => false,
            'message' => "Sukses",
            'data' => $order->load('products')
        ]);
    }

    public function show(Order $order){

        return response()->json([
            'error' => false,
            'message' => "Sukses",
            'data' => $order->load('products', 'packages')
        ]);
    }

    public function buktiUpdate(Request $request, Order $order){
        $validator = Validator::make($request->all(), [
            'atas_nama_rekening' => 'required|max:255|string',
            'image_bukti' => 'required|mimes:jpeg,bmp,png,jpg|max:5000'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => 'The given data was invalid',
                'data'=> $this->transform($validator)
            ], 422);
        }

        $order->atas_nama_rekening = $request->atas_nama_rekening;
        $image_path = $request->file('image_bukti')->store('public/orders/images');
        $order->image_bukti = "/naturalisme/public" .  Storage::url($image_path);
        $order->image_bukti_path = $image_path;
        $order->status = "Menunggu Konfirmasi";
        $order->save();

        return response()->json([
            'error' => false,
            'message' => "Sukses",
            'data' => $order
        ]);
    }

    public function destroy(Order $order){
        $order->products()->detach();
        $order->delete();

        return response()->json([
            'error' => false,
            'message' => "Sukses",
            'data' => $order
        ]);
    }

    private function transform($validator)
    {
        $response = [];
        foreach ($validator->messages()->toArray() as $key => $value) {
            $response[$key] = $value[0]; 
        }

        return $response;
    }
}
