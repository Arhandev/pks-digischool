<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';
    protected $primaryKey = "id";
    protected $keyTypes = "string";
    protected $fillable = [
        'first_name',
        'last_name',
        'image',
        'image_hover',
        'perawatan',
        'jenis',
        'air',
        'harga',
        'category_id',
    ];
    public $incrementing = false;
    public $timestamps = true;

    protected static function boot()
    {
        parent::boot();


        static::creating(function($model){
            if(empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
            $model->user_id = auth()->user()->id;
        });
    }

    public function category(){
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    //laravel accesor
    public function getImageAttribute($value)
    {
        return $value ? url($value) : $value;
    }

    //laravel accesor
    public function getImageHoverAttribute($value)
    {
        return $value ? url($value) : $value;
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_has_products', 'order_id', 'product_id')->withPivot('qty');
    }
}
