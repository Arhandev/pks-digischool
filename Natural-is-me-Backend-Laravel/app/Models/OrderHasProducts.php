<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class OrderHasProducts extends Model
{
    use HasFactory;

    protected $table = 'order_has_products';
    protected $primaryKey = "id";
    protected $keyTypes = "string";
    protected $fillable = [
        'product_id',
        'package_id',
        'order_id',
        'qty'
    ];
    public $incrementing = false;
    public $timestamps = true;

    protected static function boot()
    {
        parent::boot();


        static::creating(function($model){
            if(empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
            $model->user_id = auth()->user()->id;
        });
    }
}
